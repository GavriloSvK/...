# Spustenie projektu

1. Spustite docker-start.sh v priečinku **docker**
2. Nastavte si local.neon podľa .dist súboru
3. Spustite tieto commandy v rovnakom poradí.


   `composer install`
   
   `npm install`
   
   `gulp`

   `php bin/console migrations:create`

   
