<?php











namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;








class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'nette/sandbox',
  ),
  'versions' => 
  array (
    'contributte/application' => 
    array (
      'pretty_version' => 'v0.5.1',
      'version' => '0.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2579ab2bc3b7c95ae32a2e664ac9a8cc038777f9',
    ),
    'contributte/console' => 
    array (
      'pretty_version' => 'v0.9.1',
      'version' => '0.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '549893573ba3cb81f476785763f48178b5166322',
    ),
    'contributte/di' => 
    array (
      'pretty_version' => 'v0.5.1',
      'version' => '0.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '534fdb5e85b4ae01f8f848fc4b752deb8458ed7c',
    ),
    'contributte/forms-multiplier' => 
    array (
      'pretty_version' => 'v3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7854344b3bc6c363a0dd31daa36bc7c9dfa4265',
    ),
    'dg/adminer-custom' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '56d09f23836002fd9d32cfd2817f001178d1bbc2',
    ),
    'dg/dibi' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'dibi/dibi' => 
    array (
      'pretty_version' => 'v4.2.2',
      'version' => '4.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c3f72a1791e0efe93a511664f7b5444398ca36',
    ),
    'latte/latte' => 
    array (
      'pretty_version' => 'v2.10.3',
      'version' => '2.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd2c58863e830c22a2c6b85af63f37b829d255df',
    ),
    'nette/application' => 
    array (
      'pretty_version' => 'v3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f817a0b738a3190efe1e573a099d1a80797e156b',
    ),
    'nette/bootstrap' => 
    array (
      'pretty_version' => 'v3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'efe6c30fc009451f59fe56f3b309eb85c48b2baf',
    ),
    'nette/caching' => 
    array (
      'pretty_version' => 'v3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3e771c589dee414724be473c24ad16dae50c1960',
    ),
    'nette/component-model' => 
    array (
      'pretty_version' => 'v3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '66409cf5507c77edb46ffa88cf6a92ff58395601',
    ),
    'nette/database' => 
    array (
      'pretty_version' => 'v3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '8caf46857ea50784734e0d53e4accf3833d236ff',
    ),
    'nette/di' => 
    array (
      'pretty_version' => 'v3.0.8',
      'version' => '3.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a3210f0f1f971db8a6e970c716c1cebd28b7ab0',
    ),
    'nette/finder' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ad2c298eb8c687dd0e74ae84206a4186eeaed50',
    ),
    'nette/forms' => 
    array (
      'pretty_version' => 'v3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bfbef39b1f07dc5cdfeb7dba4843ee086ca0c065',
    ),
    'nette/http' => 
    array (
      'pretty_version' => 'v3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c903d0f0b793ed2045a442f338e756e1d3954c22',
    ),
    'nette/mail' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd5035afcb733ff76b353b8a126a1a6fd560005de',
    ),
    'nette/neon' => 
    array (
      'pretty_version' => 'v3.2.2',
      'version' => '3.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e4ca6f4669121ca6876b1d048c612480e39a28d5',
    ),
    'nette/php-generator' => 
    array (
      'pretty_version' => 'v3.5.3',
      'version' => '3.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '119f01a7bd590469cb01b538f20a125a28853626',
    ),
    'nette/robot-loader' => 
    array (
      'pretty_version' => 'v3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3973cf3970d1de7b30888fd10b92dac9e0c2fd82',
    ),
    'nette/routing' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5532e7e3612e13def357f089c1a5c25793a16843',
    ),
    'nette/safe-stream' => 
    array (
      'pretty_version' => 'v2.4.1',
      'version' => '2.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae6f314041892138cb38b4ff8782e92e03750918',
    ),
    'nette/sandbox' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'nette/schema' => 
    array (
      'pretty_version' => 'v1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5ed39fc96358f922cedfd1e516f0dadf5d2be0d',
    ),
    'nette/security' => 
    array (
      'pretty_version' => 'v3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '817ee98aad1f122f8f40b728c35e634086e1093d',
    ),
    'nette/tester' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0988c33459b49bfd6c8d06e16f29de96eba341e7',
    ),
    'nette/utils' => 
    array (
      'pretty_version' => 'v3.2.2',
      'version' => '3.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '967cfc4f9a1acd5f1058d76715a424c53343c20c',
    ),
    'nextras/migrations' => 
    array (
      'pretty_version' => 'v3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6d5983de55cc51124012153477f6aa07440c935b',
    ),
    'phpstan/phpstan' => 
    array (
      'pretty_version' => '0.12.88',
      'version' => '0.12.88.0',
      'aliases' => 
      array (
      ),
      'reference' => '464d1a81af49409c41074aa6640ed0c4cbd9bb68',
    ),
    'phpstan/phpstan-nette' => 
    array (
      'pretty_version' => '0.12.19',
      'version' => '0.12.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '4be612eb635f69e8e7b8837a2b39b4c68a03e5a7',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '058553870f7809087fa80fa734704a21b9bcaeb2',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '24b72c6baa32c746a4d0840147c9715e42bb68ab',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2df51500adbaebdc4c38dea4c89a2e131c45c8a1',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eca0bf41ed421bed1b57c4958bab16aa86b757d0',
    ),
    'symfony/property-access' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8988399a556cffb0fba9bb3603f8d1ba4543eceb',
    ),
    'symfony/property-info' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '64d7dfb197749b0e00c6d8dcd5629fd6fac352f7',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9a0f8b6aafc5d2d1c116dcccd1573a95153515b',
    ),
    'symfony/thanks' => 
    array (
      'pretty_version' => 'v1.2.10',
      'version' => '1.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e9c4709560296acbd4fe9e12b8d57a925aa7eae8',
    ),
    'tracy/tracy' => 
    array (
      'pretty_version' => 'v2.8.4',
      'version' => '2.8.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb7d3dcd9469aa2aa6722edf6bee2d5d75188079',
    ),
    'ublaboo/datagrid' => 
    array (
      'pretty_version' => 'v6.8.1',
      'version' => '6.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab91aad64201e56ae3b0b28d0805cb60973e98aa',
    ),
  ),
);
private static $canGetVendors;
private static $installedByVendor = array();







public static function getInstalledPackages()
{
$packages = array();
foreach (self::getInstalled() as $installed) {
$packages[] = array_keys($installed['versions']);
}

if (1 === \count($packages)) {
return $packages[0];
}

return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
}









public static function isInstalled($packageName)
{
foreach (self::getInstalled() as $installed) {
if (isset($installed['versions'][$packageName])) {
return true;
}
}

return false;
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

$ranges = array();
if (isset($installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = $installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['version'])) {
return null;
}

return $installed['versions'][$packageName]['version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getPrettyVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return $installed['versions'][$packageName]['pretty_version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getReference($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['reference'])) {
return null;
}

return $installed['versions'][$packageName]['reference'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getRootPackage()
{
$installed = self::getInstalled();

return $installed[0]['root'];
}








public static function getRawData()
{
@trigger_error('getRawData only returns the first dataset loaded, which may not be what you expect. Use getAllRawData() instead which returns all datasets for all autoloaders present in the process.', E_USER_DEPRECATED);

return self::$installed;
}







public static function getAllRawData()
{
return self::getInstalled();
}



















public static function reload($data)
{
self::$installed = $data;
self::$installedByVendor = array();
}





private static function getInstalled()
{
if (null === self::$canGetVendors) {
self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
}

$installed = array();

if (self::$canGetVendors) {
foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
if (isset(self::$installedByVendor[$vendorDir])) {
$installed[] = self::$installedByVendor[$vendorDir];
} elseif (is_file($vendorDir.'/composer/installed.php')) {
$installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
}
}
}

$installed[] = self::$installed;

return $installed;
}
}
