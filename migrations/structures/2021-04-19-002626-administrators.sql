-- Adminer 4.8.0 MySQL 5.5.5-10.3.7-MariaDB-1:10.3.7+maria~xenial-log dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators`
(
    `administrator_id` int(11)      NOT NULL AUTO_INCREMENT,
    `email`            varchar(50)  NOT NULL,
    `password`         varchar(255) NOT NULL,
    PRIMARY KEY (`administrator_id`),
    UNIQUE KEY `email` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
-- 2021-04-18 22:27:03