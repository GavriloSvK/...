-- Adminer 4.8.0 MySQL 5.5.5-10.5.2-MariaDB-1:10.5.2+maria~bionic dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`
(
    `question_id`        int(11)      NOT NULL AUTO_INCREMENT,
    `created`            datetime     NOT NULL,
    `soft_state`         char(1)      NOT NULL,
    `parent_question_id` int(11) DEFAULT NULL,
    `question_type`      char(1)      NOT NULL,
    `question_author`    char(1)      NOT NULL,
    `question`           text         NOT NULL,
    `reason_of_answer`   text         NOT NULL,
    `source_of_answer`   varchar(255) NOT NULL,
    PRIMARY KEY (`question_id`),
    KEY `parent_question_id` (`parent_question_id`),
    CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`parent_question_id`) REFERENCES `questions` (`question_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;


-- 2021-04-26 09:48:57

CREATE TABLE `question_answers`
(
    `question_answer_id` int        NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `question_id`        int(11)    NOT NULL,
    `answer`             text       NOT NULL,
    `is_correct`         tinyint(1) NOT NULL,
    FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
);