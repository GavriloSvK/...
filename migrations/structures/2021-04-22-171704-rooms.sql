CREATE TABLE `rooms`
(
    `room_id`      int          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `created`      datetime     NOT NULL,
    `soft_state`   char(1)      NOT NULL,
    `name`         varchar(255) NOT NULL,
    `password`     varchar(255) NOT NULL,
    `is_test_room` tinyint(1)   NOT NULL
);

ALTER TABLE `rooms`
    ADD UNIQUE `password` (`password`);