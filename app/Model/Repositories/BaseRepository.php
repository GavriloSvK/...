<?php

namespace App\Model\Repositories;

use App;
use Dibi\Connection;
use Dibi\Exception;
use Dibi\Fluent;
use Dibi\Row;
use Nette\Forms\IControl;
use Nette\NotImplementedException;
use Nette\Utils\Strings;
use Nette\Utils\Validators;

abstract class BaseRepository
{

	/** @var Connection */
	protected $db;

	/** @var string Primary key name */
	protected $primaryKey = NULL;

	/** @var string Table name */
	protected $table = NULL;

	/** @var string Table alias */
	protected $alias = NULL;

	abstract protected function setup();

	public function __construct(Connection $db)
	{
		$this->db = $db;

		$this->setup();

		Validators::assert($this->table, 'string', 'Table name');
		Validators::assert($this->primaryKey, 'string', 'Primary key');
		Validators::assert($this->alias, 'string', 'Alias');
	}

	public function startTransaction()
	{
		$this->db->begin();
	}

	public function commitTransaction()
	{
		$this->db->commit();
	}

	public function rollbackTransaction()
	{
		$this->db->rollback();
	}

	public function getDb()
	{
		return $this->db;
	}

	/**
	 * @param $name
	 * @param $arguments
	 *
	 * @return array|bool|Fluent|Row|null
	 */
	public function __call($name, $arguments)
	{
		// first we look if $name matches to exists or existsBy* function
		$matches = Strings::match($name, '^existsBy(.*)^');

		if ($name == 'exists' || is_countable($matches) and count($matches) == 2) {
			if ($name == 'exists') {
				$column = $this->primaryKey;
				$value = $arguments[0];
				$invert = FALSE;
				$exclude = FALSE;
			} else {
				$column = Strings::lower($matches[1]);
				if (count($arguments) == 2) {
					[$control, $extra] = $arguments;
					if ($control instanceof IControl) {
						$value = $control->value;
						$invert = TRUE;
					} else {
						$value = $control;
						$invert = FALSE;
					}
				} else {
					$value = $arguments[0];
					$extra = NULL;
					$invert = FALSE;
				}
				$exclude = ($extra === NULL) ? FALSE : $extra;
			}

			if ($value == '') { // dont check for existence of empty values
				return TRUE;
			}

			$query = $this->db->select('COUNT(*)')
				->from('%n', $this->table)
				->where('%n = %s', $column, $value);

			if ($exclude) {
				if (isset($exclude['invert'])) {
					$invert = FALSE;
				} else {
					$query->where('%n != %s', $this->primaryKey, $exclude);
				}
			}

			$result = (bool)$query->fetchSingle();

			return ($invert) ? !$result : $result;
		}

		// otherwise we check if it is findOneBy*
		$matches = Strings::match($name, '^findOneBy(.*)^');
		if (count($matches) == 2) {
			$column = Strings::lower($matches[1]);

			return $this->find($arguments[0], $column);
		}

		// else we check if it is findAllBy*
		$matches = Strings::match($name, '^findAllBy(.*)^');
		if (count($matches) == 2) {
			$column = Strings::lower($matches[1]);

			return $this->db->select('*')
				->from($this->table)
				->where('%n = %s', $column, $arguments[0]);
		}

		throw new NotImplementedException("Method '" . get_called_class() . "::$name' does not exist!");
	}

	/**
	 * @param int $id
	 *
	 * @throws Exception
	 */
	public function hardDeleteById(int $id)
	{
		$this->db->delete($this->table)
			->where('%n = %i', $this->primaryKey, $id)
			->execute();
	}

	/**
	 * @param array $conditions
	 *
	 * @throws Exception
	 */
	public function hardDeleteByConditions(array $conditions)
	{
		$this->db->delete($this->table)
			->where($conditions)
			->execute();
	}

	/**
	 * @throws Exception
	 */
	public function deleteAll()
	{
		$this->db->delete($this->table)->execute();
	}

	/**
	 * @param $values
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function insert($values)
	{
		$this->db->insert($this->table, $values)
			->execute();

		return $this->db->getInsertId();
	}

	/**
	 * @param $id
	 * @param $values
	 *
	 * @throws Exception
	 */
	public function update($id, $values)
	{
		$this->db->update($this->table, $values)
			->where('%n = %i', $this->primaryKey, $id)
			->execute();
	}

	/**
	 * @param      $id
	 * @param bool $column
	 *
	 * @return array|Row|null
	 */
	public function find($id, $column = FALSE)
	{
		return $this->db->select('*')
			->from('%n', $this->table)
			->where(
				'%n = %s',
				($column === FALSE) ? $this->primaryKey : $column,
				$id
			)
			->fetch();
	}

	/**
	 * @param string $appendSelect
	 *
	 * @return Fluent
	 */
	public function findAll($appendSelect = '')
	{
		if ($appendSelect != '') {
			$appendSelect = ', ' . $appendSelect;
		}

		return $this->db->select('%n.*' . $appendSelect, $this->alias)
			->from('%n %n', $this->table, $this->alias);
	}

}
