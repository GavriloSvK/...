<?php

namespace App\Model\Repositories;

use App;
use Nette\Utils\DateTime;

class RoomRepository extends BaseRepository
{

	protected function setup()
	{
		$this->table = 'rooms';
		$this->primaryKey = 'room_id';
		$this->alias = 'ro';
	}

}
