<?php

namespace App\Model\Repositories;

use App;
use Nette\Utils\DateTime;

class QuestionRepository extends BaseRepository
{

	protected function setup()
	{
		$this->table = 'questions';
		$this->primaryKey = 'question_id';
		$this->alias = 'qu';
	}

}
