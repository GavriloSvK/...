<?php

namespace App\Model\Repositories;

use App;
use Nette\Utils\DateTime;

class AdministratorRepository extends BaseRepository
{

	protected function setup()
	{
		$this->table = 'administrators';
		$this->primaryKey = 'administrator_id';
		$this->alias = 'ad';
	}

}
