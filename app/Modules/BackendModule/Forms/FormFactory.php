<?php

declare(strict_types=1);

namespace App\Modules\BackendModule\Forms;

use Nette\Application\UI\Form;
use Nette\Forms\Controls\Button;
use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextBase;
use Nette\Utils\Html;

/**
 * Class FormFactory
 * @package App\BackendModule\Forms
 */
class FormFactory
{

	/**
	 * @return Form
	 */
	public function create(): Form
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div class="kt-portlet__body"';

		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.required'] = NULL;

		$renderer->wrappers['label']['container'] = NULL;
		$renderer->wrappers['label']['requiredsuffix'] = ' *';
		$renderer->wrappers['label']['suffix'] = ':';

		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['control']['errorcontainer'] = 'div class="invalid-feedback"';
		$renderer->wrappers['control']['description'] = 'span class="form-text text-muted"';
		$renderer->wrappers['control']['.text'] = 'form-control form-control-sm';
		$renderer->wrappers['control']['.url'] = 'form-control form-control-sm';
		$renderer->wrappers['control']['.email'] = 'form-control form-control-sm';
		$renderer->wrappers['control']['.password'] = 'form-control form-control-sm';
		$renderer->wrappers['control']['.number'] = 'form-control form-control-sm';
		$renderer->wrappers['control']['.file'] = 'form-control form-control-sm';
		$renderer->wrappers['control']['.error'] = 'is-invalid';
		$renderer->wrappers['control']['.button'] = 'btn btn-brand';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary mr-2';

		$form->getElementPrototype()->class('kt-form kt-form--label-right');

		foreach ($form->getControls() as $control) {
			if ($control instanceof TextBase || $control instanceof SelectBox || $control instanceof MultiSelectBox) {
				$control->getControlPrototype()->addClass('form-control form-control-sm');
			}
		}


		return $form;
	}

}
