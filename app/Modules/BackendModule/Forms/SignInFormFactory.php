<?php
declare(strict_types=1);

namespace App\Modules\BackendModule\Forms;

use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use App\Security\BackendAuthenticator;
use Nette\Security\AuthenticationException;

/**
 * Class SignInFormFactory
 * @package App\AdminModule\Forms
 */
class SignInFormFactory
{

	/** @var FormFactory */
	private FormFactory $factory;

	/** @var User */
	private User $user;

	private BackendAuthenticator $backendAuthenticator;

	/**
	 * SignInFormFactory constructor.
	 *
	 * @param \App\Modules\BackendModule\Forms\FormFactory $factory
	 * @param \Nette\Security\User                         $user
	 * @param \App\Security\BackendAuthenticator           $backendAuthenticator
	 */
	public function __construct(FormFactory $factory, User $user, BackendAuthenticator $backendAuthenticator)
	{
		$this->factory = $factory;
		$this->user = $user;
		$this->backendAuthenticator = $backendAuthenticator;
	}


	/**
	 * @param callable $onSuccess
	 * @return Form
	 */
	public function create(callable $onSuccess): Form
	{
		$form = $this->factory->create();

		$form->addEmail('email', 'Email')
			->setRequired('Prosím vložte email.');

		$form->addPassword('password', 'Heslo')
			->setRequired('Prosím vložte přihlašovací heslo.');

		$form->addSubmit('submit', 'Prihlásiť');

		/**
		 * @param Form      $form
		 * @param ArrayHash $values
		 */
		$form->onSuccess[] = function (Form $form, ArrayHash $values) use ($onSuccess) {
			try {
				$this->user->setAuthenticator($this->backendAuthenticator);
				$this->user->login($values->email, $values->password);
			} catch (AuthenticationException $exception) {
				$form->addError('Chybné prihlasovacie údaje.');
				return;
			}
			$onSuccess();
		};
		return $form;
	}

}
