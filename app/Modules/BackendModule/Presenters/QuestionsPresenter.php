<?php

declare(strict_types=1);

namespace App\Modules\BackendModule\Presenters;

use Nette\Utils\DateTime;
use Nette\Forms\Container;
use Ublaboo\DataGrid\DataGrid;
use Nette\Application\UI\Form;
use App\Enums\QuestionTypeEnum;
use App\Enums\QuestionAuthorEnum;
use App\Model\Repositories\RoomRepository;
use Dibi\UniqueConstraintViolationException;
use App\Model\Repositories\QuestionRepository;

final class QuestionsPresenter extends BasePresenter
{

	/** @persistent */
	public ?int $question_id = NULL;

	/** @var QuestionRepository @inject */
	public QuestionRepository $questionRepository;

	public function createComponentRoomsGrid($name)
	{
		$grid = new DataGrid($this, $name);

		$grid->setPrimaryKey('question_id');
		$grid->setDataSource($this->questionRepository->findAll());
		$grid->setItemsPerPageList([50]);

		$grid->addColumnNumber('question_id', '#')
			->setAlign('left');
		$grid->addFilterText('question_id', '#');

		$grid->addColumnText('question_type', 'Typ otázky')
			->setAlign('left')
			->setRenderer(function ($item) {
				return QuestionTypeEnum::getItems()[$item->question_type];
			});

		$grid->addColumnText('question_author', 'Autor otázky')
			->setAlign('left')
			->setRenderer(function ($item) {
				return QuestionAuthorEnum::getItems()[$item->question_author];
			});

		$grid->addColumnText('question', 'Otázka')
			->setAlign('left');
		$grid->addFilterText('question', 'Otázka');

		$grid->addColumnDateTime('created', 'Vytvorená')
			->setFormat('j.n.Y H:i:s')
			->setAlign('left');

		$grid->addToolbarButton('questionVariant', 'Vytvoriť otázku');
	}

	protected function createComponentQuestionVariantForm(): Form
	{
		$form = new Form;
		$form->addSelect('type_question', 'Typ otázky', QuestionTypeEnum::getItems());
		$form->addSubmit('submit');

		$form->onSuccess[] = [$this, 'questionVariantFormSucceeded'];

		return $form;
	}

	public function questionVariantFormSucceeded(Form $form, $data)
	{
		$this->redirect('add', ['typeQuestion' => $data->type_question]);
	}

	protected function createComponentQuestionForm()
	{
		$form = new Form;

		$form->addTextArea('question', 'Otázka');
		$form->addTextArea('reason_of_answer', 'Odôvodnenie odpovede');
		$form->addTextArea('source_of_answer', 'Zdroj');

		if ($this->getParameter('typeQuestion') == QuestionTypeEnum::TRUE_OR_FALSE) {
			$form->addCheckbox('is_correct', 'Je odpoveď na otazku ANO ?');
		}

		if ($this->getParameter('typeQuestion') == QuestionTypeEnum::ONE_CHOICE || $this->getParameter('typeQuestion') == QuestionTypeEnum::MORE_CHOICES) {
			$copies = 1;
			$maxCopies = 10;

			$multiplier = $form->addMultiplier('multiplier', function (Container $container, $form) {
				$container->addText('answer', 'Odpoveď');
				$container->addCheckbox('answer_correct', 'Je toto správna odpoveď ?');
			}, $copies, $maxCopies);

			$multiplier->addCreateButton('Add')
				->addClass('btn btn-primary');

			$multiplier->addRemoveButton('Remove')
				->addClass('btn btn-danger');
		}



		$form->addSubmit('submit');


//		if ($this->presenter->getAction() === 'add') {
//			$form->onSuccess[] = [$this, 'roomFormCreateSucceeded'];
//		} elseif ($this->presenter->getAction() === 'edit') {
//			$form->onSuccess[] = [$this, 'roomFormEditSucceeded'];
//		}
		return $form;
	}

//	public function roomFormCreateSucceeded(Form $form, $data): void
//	{
//
//		try {
//			$this->roomRepository->insert([
//				'created' => new DateTime(),
//				'soft_state' => 'E',
//				'name' => $data->name,
//				'password' => $data->password,
//				'is_test_room' => $data->is_test_room,
//			]);
//		} catch (UniqueConstraintViolationException $exception) {
//			$form->addError('zvoľ iné heslo');
//			return;
//		}
//
//		$this->flashMessage('Room bola uspesne vytvorena');
//		$this->redirect('Rooms:');
//	}
//
//	public function roomFormEditSucceeded(Form $form, $data): void
//	{
//		try {
//			$this->roomRepository->update($this->room_id, [
//				'soft_state' => 'E',
//				'name' => $data->name,
//				'password' => $data->password,
//				'is_test_room' => $data->is_test_room,
//			]);
//		} catch (UniqueConstraintViolationException $exception) {
//			$form->addError('zvoľ iné heslo');
//			return;
//		}
//
//		$this->flashMessage('Room bola uspesne editovana');
//		$this->redirect('Rooms:');
//	}

	public function actionAdd($typeQuestion = NULL)
	{
	}

}
