<?php
declare(strict_types=1);

namespace App\Modules\BackendModule\Presenters;

use App\Modules\BackendModule\Forms\SignInFormFactory;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;

final class SignPresenter extends BasePresenter
{

	/** @var SignInFormFactory @inject */
	public SignInFormFactory $singInForm;


	/**
	 * Login form
	 *
	 * @return Form
	 */
	protected function createComponentSignInForm(): Form
	{
		return $this->singInForm->create(function () {
			$this->flashMessage('Boli ste úspešne prihlasený. Vašu aktivitu v systéme zaznamenávame.', self::FLASH_SUCCESS);
			$this->redirect('Homepage:Default');
		});
	}

	public function renderIn(): void
	{
		if ($this->user->isLoggedIn() === TRUE) {
			$this->flashMessage('Pred opätovným prihlásením sa najprv musíte odhlásiť');
			$this->redirect('Homepage:default');
		}
	}

	/**
	 * Action for logout
	 *
	 * @throws AbortException
	 */
	public function actionOut(): void
	{
		$this->user->logout();
		$this->flashMessage('Boli ste úspešne odhlasení.');
		$this->redirect('Sign:in');
	}

}
