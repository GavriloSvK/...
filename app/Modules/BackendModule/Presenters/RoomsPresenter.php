<?php

declare(strict_types=1);

namespace App\Modules\BackendModule\Presenters;

use Nette\Utils\DateTime;
use Ublaboo\DataGrid\DataGrid;
use Nette\Application\UI\Form;
use App\Model\Repositories\RoomRepository;
use Dibi\UniqueConstraintViolationException;

final class RoomsPresenter extends BasePresenter
{

	/** @persistent  */
	public ?int $room_id = NULL;

	/** @var RoomRepository @inject */
	public RoomRepository $roomRepository;

	public function createComponentRoomsGrid($name)
	{
		$grid = new DataGrid($this, $name);

		$grid->setPrimaryKey('room_id');
		$grid->setDataSource($this->roomRepository->findAll());
		$grid->setItemsPerPageList([50]);

		$grid->addColumnNumber('room_id', '#')
			->setAlign('left');
		$grid->addFilterText('room_id', '#');

		$grid->addColumnText('name', 'Názov')
			->setAlign('left');
		$grid->addFilterText('name', 'Názov');

		$grid->addColumnText('password', 'Heslo')
			->setAlign('left');
		$grid->addFilterText('password', 'Heslo');

		$grid->addColumnStatus('is_test_room', 'Demo miestnosť ?')
			->setCaret(TRUE)
			->addOption(1, 'Áno')
			->setClass('btn-success')
			->endOption()
			->addOption(0, 'Nie')
			->setClass('btn-danger')
			->endOption();

		$grid->addColumnDateTime('created', 'Vytvorená')
			->setFormat('j.n.Y H:i:s')
			->setAlign('left');

		$grid->addAction('edit', 'edit',  'Rooms:edit');
	}

	protected function createComponentRoomForm(): Form
	{
		$form = new Form;
		$form->addText('name', 'Nazov');
		$form->addText('password', 'Heslo:');
		$form->addCheckbox('is_test_room', 'Testovacia miestnost ?');

		if ($this->presenter->getAction() === 'add') {
			$form->onSuccess[] = [$this, 'roomFormCreateSucceeded'];
		} elseif ($this->presenter->getAction() === 'edit') {
			$form->onSuccess[] = [$this, 'roomFormEditSucceeded'];
		}
		return $form;
	}

	public function roomFormCreateSucceeded(Form $form, $data): void
	{

		try {
			$this->roomRepository->insert([
				'created' => new DateTime(),
				'soft_state' => 'E',
				'name' => $data->name,
				'password' => $data->password,
				'is_test_room' => $data->is_test_room,
			]);
		} catch (UniqueConstraintViolationException $exception) {
			$form->addError('zvoľ iné heslo');
			return;
		}

		$this->flashMessage('Room bola uspesne vytvorena');
		$this->redirect('Rooms:');
	}

	public function roomFormEditSucceeded(Form $form, $data): void
	{
		try {
			$this->roomRepository->update($this->room_id, [
				'soft_state' => 'E',
				'name' => $data->name,
				'password' => $data->password,
				'is_test_room' => $data->is_test_room,
			]);
		} catch (UniqueConstraintViolationException $exception) {
			$form->addError('zvoľ iné heslo');
			return;
		}

		$this->flashMessage('Room bola uspesne editovana');
		$this->redirect('Rooms:');
	}

	public function renderEdit()
	{
		$room = $this->roomRepository->find($this->room_id);

		if ($room === NULL) {
			$this->error();
		}

		$this['roomForm']->setDefaults([
			'name' => $room->name,
			'password' => $room->password,
			'is_test_room' => $room->is_test_room,
		]);

		$this['roomForm']->addSubmit('submit', 'Upraviť');
	}

	public function renderAdd()
	{
		$this['roomForm']->addSubmit('submit', 'Vytvoriť');
	}

}
