<?php

declare(strict_types=1);

namespace App\Modules\BackendModule\Presenters;

use Nette\Utils\DateTime;
use Nette\Forms\Container;
use Ublaboo\DataGrid\DataGrid;
use Nette\Application\UI\Form;
use App\Enums\QuestionTypeEnum;
use App\Enums\QuestionAuthorEnum;
use App\Model\Repositories\RoomRepository;
use Dibi\UniqueConstraintViolationException;
use App\Model\Repositories\QuestionRepository;

final class CollectionQuestionsPresenter extends BasePresenter
{

}
