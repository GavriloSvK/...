<?php

declare(strict_types=1);

namespace App\Modules\BackendModule\Presenters;

final class HomepagePresenter extends BasePresenter
{

    protected function startup()
    {
        parent::startup();

        if ($this->user->isLoggedIn() === false){
            $this->flashMessage('Pred pokracovanim sa musite prihlasit');
            $this->redirect('Sign:in');
        }
    }

}
