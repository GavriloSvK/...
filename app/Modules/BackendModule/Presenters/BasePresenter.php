<?php

declare(strict_types=1);

namespace App\Modules\BackendModule\Presenters;

use App\Facade\G2aFacade;
use App\Latte\AgoFilter;
use App\Latte\ImageFilter;
use Nette;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	public const
		FLASH_SUCCESS = 'success',
		FLASH_WARNING = 'danger',
		FLASH_INFO = 'info';

	protected function startup()
	{
		parent::startup();

//		if ($this->user->isLoggedIn() === false){
//			$this->flashMessage('Pred pokracovanim sa musite prihlasit');
//			$this->redirect('Sign:in');
//		}
	}

}
