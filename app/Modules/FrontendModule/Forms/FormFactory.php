<?php

declare(strict_types=1);

namespace App\Modules\FrontendModule\Forms;

use Nette\Application\UI\Form;
use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextBase;


class FormFactory
{

	/**
	 * @return Form
	 */
	public function create(): Form
	{
		$form = new Form();

		foreach ($form->getControls() as $control) {
			if ($control instanceof TextBase || $control instanceof SelectBox || $control instanceof MultiSelectBox) {
				$control->getControlPrototype()->addClass('form-control');
			}
		}

		return $form;
	}

}
