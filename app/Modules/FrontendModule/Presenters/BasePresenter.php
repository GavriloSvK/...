<?php

declare(strict_types=1);

namespace App\Modules\FrontendModule\Presenters;

use Nette;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	public const
		FLASH_SUCCESS = 'success',
		FLASH_WARNING = 'danger',
		FLASH_INFO = 'info';
}
