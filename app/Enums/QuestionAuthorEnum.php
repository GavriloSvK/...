<?php

namespace App\Enums;

use App;
use Nette;

class QuestionAuthorEnum
{

	use Nette\SmartObject;

	public const
		ADMIN = 'A';

	public static function getItems()
	{
		$items = [
			static::ADMIN => 'Administrátor',
		];

		return $items;
	}

}

