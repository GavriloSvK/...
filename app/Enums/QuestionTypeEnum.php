<?php

namespace App\Enums;

use App;
use Nette;

class QuestionTypeEnum
{

	use Nette\SmartObject;

	public const
		TRUE_OR_FALSE = 'B',
		ONE_CHOICE = 'O',
		MORE_CHOICES = 'M',
		STUDY = 'S';

	public static function getItems()
	{
		$items = [
			static::TRUE_OR_FALSE => 'Pravda - Nepravda',
			static::ONE_CHOICE => 'Jedna správna možnosť',
			static::MORE_CHOICES => 'Viac správnych možností',
			static::STUDY => 'Prípadová štúdia',
		];

		return $items;
	}

}

