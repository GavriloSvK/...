<?php

namespace App\Enums;

use App;
use Nette;

class SoftStatesEnum
{

	use Nette\SmartObject;

	public const
		ENABLED = 'E',
		DISABLED = 'D',
		REMOVED = 'R';

	public static function getItems($includeRemoved = true)
	{
		$items = [
			static::ENABLED => 'Zapnutý',
			static::DISABLED => 'Vypnutý',
			static::REMOVED => 'Zmazaný',
		];

		if ($includeRemoved == false) {
			unset($items[self::REMOVED]);
		}

		return $items;
	}

}

