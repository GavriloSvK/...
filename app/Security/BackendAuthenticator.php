<?php
declare(strict_types=1);

namespace App\Security;

use Nette\Security\Authenticator;
use Nette\Security\SimpleIdentity;
use App\Model\Repositories\AdministratorRepository;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;
use Nette\SmartObject;

class BackendAuthenticator implements Authenticator
{

	use SmartObject;

	/** @var AdministratorRepository */
	private AdministratorRepository $administratorRepository;

	/** @var Passwords */
	private Passwords $passwords;

	/**
	 * Authenticator constructor.
	 *
	 * @param AdministratorRepository $administratorRepository
	 * @param Passwords               $passwords
	 */
	public function __construct(AdministratorRepository $administratorRepository, Passwords $passwords)
	{
		$this->administratorRepository = $administratorRepository;
		$this->passwords = $passwords;
	}

	public function authenticate(string $user, string $password): SimpleIdentity
	{
		$administrator = $this->administratorRepository->findAll()->where('[ad.email] = %s', $user)->fetch();

		if ($administrator == NULL) {
			throw new AuthenticationException('Email je nesprávny.', self::IDENTITY_NOT_FOUND);
		} elseif (!$this->passwords->verify($password, (string)$administrator->password)) {
			throw new AuthenticationException('Prihlasovacie heslo je nesprávne', self::INVALID_CREDENTIAL);
		}

		return new SimpleIdentity($administrator->administrator_id, NULL, [
			'email' => $administrator->email,
		]);
	}
}
