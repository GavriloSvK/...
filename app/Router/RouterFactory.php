<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

    public static function createRouter(): RouteList
    {
        $router = new RouteList;

        $router[] = $backendRouter = new RouteList('Backend');
        $backendRouter[] = new Route('prihlasenie', 'Sign:in');
        $backendRouter[] = new Route('zabudnute-heslo', 'Sign:lost');
        $backendRouter[] = new Route('uzivatelia', 'Users:default');
        $backendRouter[] = new Route('admin/<presenter>/<action>[/<id>]', 'Homepage:default');


        $router[] = $frontendRouter = new RouteList('Frontend');
        $frontendRouter[] = new Route('<presenter>/<action>[/<id [0-9]+>]', 'Homepage:default');

        return $router;
    }
}
